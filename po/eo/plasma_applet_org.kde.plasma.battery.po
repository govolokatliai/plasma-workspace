# translation of plasma_applet_org.kde.plasma.battery.pot to esperanto
# Copyright (C) 2007 Free Software Foundation, Inc.
# This file is distributed under the same license as the plasma-workspace package.
# Pierre-Marie Pédrot <pedrotpmx@wanadoo.fr>, 2007, 2008.
# Cindy McKee <cfmckee@gmail.com>, 2007, 2008, 2009.
# Markus Sunela <markus.sunela@tut.fi>, 2009.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_battery\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-07 00:38+0000\n"
"PO-Revision-Date: 2024-02-09 22:05+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: eo <kde-i18n-doc@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/ui/BatteryItem.qml:107
#, kde-format
msgctxt "Placeholder is battery percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:171
#, kde-format
msgid ""
"This battery's health is at only %1% and it should be replaced. Contact the "
"manufacturer."
msgstr ""
"La sano de ĉi tiu baterio estas je nur %1% kaj ĝi devus esti anstataŭigita. "
"Kontaktu la fabrikiston."

#: package/contents/ui/BatteryItem.qml:191
#, kde-format
msgid "Time To Full:"
msgstr "Tempo por Plenigi:"

#: package/contents/ui/BatteryItem.qml:192
#, kde-format
msgid "Remaining Time:"
msgstr "Restanta tempo:"

#: package/contents/ui/BatteryItem.qml:197
#, kde-format
msgctxt "@info"
msgid "Estimating…"
msgstr "Taksante…"

#: package/contents/ui/BatteryItem.qml:209
#, kde-format
msgid "Battery Health:"
msgstr "Bateria Sano:"

#: package/contents/ui/BatteryItem.qml:215
#, kde-format
msgctxt "Placeholder is battery health percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:229
#, kde-format
msgid "Battery is configured to charge up to approximately %1%."
msgstr "Baterio estas agordita por ŝargi ĝis proksimume %1%."

#: package/contents/ui/CompactRepresentation.qml:105
#, kde-format
msgctxt "battery percentage below battery icon"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/logic.js:23 package/contents/ui/logic.js:29
#: package/contents/ui/main.qml:142
#, kde-format
msgid "Fully Charged"
msgstr "Plene Ŝargita"

#: package/contents/ui/logic.js:28
#, kde-format
msgid "Discharging"
msgstr "Malŝarganta"

#: package/contents/ui/logic.js:30
#, kde-format
msgid "Charging"
msgstr "Ŝarganta"

#: package/contents/ui/logic.js:32
#, kde-format
msgid "Not Charging"
msgstr "Ne Ŝarganta"

#: package/contents/ui/logic.js:35
#, kde-format
msgctxt "Battery is currently not present in the bay"
msgid "Not present"
msgstr "Ne ĉeestas"

#: package/contents/ui/main.qml:117
#, kde-format
msgid "Power and Battery"
msgstr "Energio kaj Baterio"

#: package/contents/ui/main.qml:117 package/contents/ui/main.qml:286
#, kde-format
msgid "Power Management"
msgstr "Energiadministrado"

#: package/contents/ui/main.qml:149
#, kde-format
msgid "Battery at %1%, not Charging"
msgstr "Baterio ĉe %1%, ne Ŝarganta"

#: package/contents/ui/main.qml:151
#, kde-format
msgid "Battery at %1%, plugged in but still discharging"
msgstr "Baterio ĉe %1%, enŝtopita sed plue malŝarganta"

#: package/contents/ui/main.qml:153
#, kde-format
msgid "Battery at %1%, Charging"
msgstr "Baterio ĉe %1%, Ŝarganta"

#: package/contents/ui/main.qml:156
#, kde-format
msgid "Battery at %1%"
msgstr "Baterio ĉe %1%"

#: package/contents/ui/main.qml:164
#, kde-format
msgid "The power supply is not powerful enough to charge the battery"
msgstr "La nutrado ne estas sufiĉe potenca por ŝargi la baterion"

#: package/contents/ui/main.qml:168
#, kde-format
msgid "No Batteries Available"
msgstr "Neniuj Baterioj Disponeblaj"

#: package/contents/ui/main.qml:174
#, kde-format
msgctxt "time until fully charged - HH:MM"
msgid "%1 until fully charged"
msgstr "%1 ĝis plenŝargita"

#: package/contents/ui/main.qml:176
#, kde-format
msgctxt "remaining time left of battery usage - HH:MM"
msgid "%1 remaining"
msgstr "%1 restanta"

#: package/contents/ui/main.qml:179
#, kde-format
msgid "Not charging"
msgstr "Ne ŝarganta"

#: package/contents/ui/main.qml:183
#, kde-format
msgid "Automatic sleep and screen locking are disabled"
msgstr "Aŭtomata dormo kaj ekranŝlosado estas malŝaltitaj"

#: package/contents/ui/main.qml:188
#, kde-format
msgid "An application has requested activating Performance mode"
msgid_plural "%1 applications have requested activating Performance mode"
msgstr[0] "Unu aplikaĵo petis aktivigon de Rendimenta reĝimo"
msgstr[1] "%1 aplikaĵoj petis aktivigon de Rendimenta reĝimo"

#: package/contents/ui/main.qml:192
#, kde-format
msgid "System is in Performance mode"
msgstr "Sistemo estas en Efikeca reĝimo"

#: package/contents/ui/main.qml:196
#, kde-format
msgid "An application has requested activating Power Save mode"
msgid_plural "%1 applications have requested activating Power Save mode"
msgstr[0] "Unu aplikaĵo petis aktivigon de Potencoŝpara reĝimo"
msgstr[1] "%1 aplikaĵoj petis aktivigon de Potencoŝpara reĝimo"

#: package/contents/ui/main.qml:200
#, kde-format
msgid "System is in Power Save mode"
msgstr "Sistemo estas en Power-Save reĝimo"

#: package/contents/ui/main.qml:260
#, kde-format
msgid "The battery applet has enabled system-wide inhibition"
msgstr "La bateri-apleto enŝaltis tutsisteman malebligon"

#: package/contents/ui/main.qml:299
#, kde-format
msgid "Failed to activate %1 mode"
msgstr "Malsukcesis aktivigi %1 reĝimon"

#: package/contents/ui/main.qml:311
#, kde-format
msgid "&Show Energy Information…"
msgstr "&Montri Energiinformon…"

#: package/contents/ui/main.qml:317
#, kde-format
msgid "Show Battery Percentage on Icon When Not Fully Charged"
msgstr "Montri Baterian Procenton sur Piktogramo Kiam Ne Plene Ŝarĝita"

#: package/contents/ui/main.qml:329
#, kde-format
msgid "&Configure Power Management…"
msgstr "&Agordi Energimastrumadon…"

#: package/contents/ui/PowerManagementItem.qml:41
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid "Manually block sleep and screen locking"
msgstr "Mane bloki dormon kaj ekranŝlosadon"

#: package/contents/ui/PowerManagementItem.qml:76
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid ""
"Your laptop is configured not to sleep when closing the lid while an "
"external monitor is connected."
msgstr ""
"Via tekkomputilo estas agordita por ne dormi kiam oni fermas la kovrilon dum "
"ekstera monitoro estas konektita."

#: package/contents/ui/PowerManagementItem.qml:87
#, kde-format
msgid "%1 application is currently blocking sleep and screen locking:"
msgid_plural "%1 applications are currently blocking sleep and screen locking:"
msgstr[0] "%1 aplikaĵo aktuale estas blokanta dormon kaj ekranŝlosadon:"
msgstr[1] "%1 aplikaĵoj aktuale estas blokantaj dormon kaj ekranŝlosadon:"

#: package/contents/ui/PowerManagementItem.qml:107
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (%2)"
msgstr "%1 aktuale estas blokanta dormon kaj ekranŝlosadon (%2)"

#: package/contents/ui/PowerManagementItem.qml:109
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (unknown reason)"
msgstr "%1 aktuale estas blokanta dormon kaj ekranŝlosadon (nekonata kialo)"

#: package/contents/ui/PowerManagementItem.qml:111
#, kde-format
msgid "An application is currently blocking sleep and screen locking (%1)"
msgstr "Aplikaĵo aktuale estas blokanta dormon kaj ekranŝlosadon (%1)"

#: package/contents/ui/PowerManagementItem.qml:113
#, kde-format
msgid ""
"An application is currently blocking sleep and screen locking (unknown "
"reason)"
msgstr ""
"Aplikaĵo aktuale estas blokanta dormon kaj ekranŝlosadon (nekonata kialo)"

#: package/contents/ui/PowerManagementItem.qml:117
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: %2"
msgstr "%1: %2"

#: package/contents/ui/PowerManagementItem.qml:119
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: unknown reason"
msgstr "%1: nekonata kialo"

#: package/contents/ui/PowerManagementItem.qml:121
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: %1"
msgstr "Nekonata aplikaĵo: %1"

#: package/contents/ui/PowerManagementItem.qml:123
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: unknown reason"
msgstr "Nekonata aplikaĵo: nekonata kialo"

#: package/contents/ui/PowerProfileItem.qml:37
#, kde-format
msgid "Power Save"
msgstr "Energiŝparo"

#: package/contents/ui/PowerProfileItem.qml:41
#, kde-format
msgid "Balanced"
msgstr "Balancita"

#: package/contents/ui/PowerProfileItem.qml:45
#, kde-format
msgid "Performance"
msgstr "Rendimento"

#: package/contents/ui/PowerProfileItem.qml:62
#, kde-format
msgid "Power Profile"
msgstr "Energioprofilo"

#: package/contents/ui/PowerProfileItem.qml:97
#, kde-format
msgctxt "Power profile"
msgid "Not available"
msgstr "Ne disponebla"

#: package/contents/ui/PowerProfileItem.qml:192
#, kde-format
msgid ""
"Performance mode has been disabled to reduce heat generation because the "
"computer has detected that it may be sitting on your lap."
msgstr ""
"Reĝimo de rendimento estis malŝaltita por redukti varmegon ĉar la komputilo "
"detektis, ke ĝi povas sidas sur via sino."

#: package/contents/ui/PowerProfileItem.qml:194
#, kde-format
msgid ""
"Performance mode is unavailable because the computer is running too hot."
msgstr "Rendimenta reĝimo ne haveblas ĉar la komputilo estas tro varma."

#: package/contents/ui/PowerProfileItem.qml:196
#, kde-format
msgid "Performance mode is unavailable."
msgstr "Rendimenta reĝimo ne haveblas."

#: package/contents/ui/PowerProfileItem.qml:209
#, kde-format
msgid ""
"Performance may be lowered to reduce heat generation because the computer "
"has detected that it may be sitting on your lap."
msgstr ""
"Efikeco povas esti malaltigita por redukti varmegon ĉar la komputilo "
"detektis, ke ĝi povas sidas sur via sino."

#: package/contents/ui/PowerProfileItem.qml:211
#, kde-format
msgid "Performance may be reduced because the computer is running too hot."
msgstr "Rendimento povas esti reduktita ĉar la komputilo estas tro varma."

#: package/contents/ui/PowerProfileItem.qml:213
#, kde-format
msgid "Performance may be reduced."
msgstr "Rendimento povas esti reduktita."

#: package/contents/ui/PowerProfileItem.qml:224
#, kde-format
msgid "One application has requested activating %2:"
msgid_plural "%1 applications have requested activating %2:"
msgstr[0] "Unu aplikaĵo petis aktivigon de %2:"
msgstr[1] "%1 aplikaĵoj petis aktivigon de %2:"

#: package/contents/ui/PowerProfileItem.qml:242
#, kde-format
msgctxt ""
"%1 is the name of the application, %2 is the reason provided by it for "
"activating performance mode"
msgid "%1: %2"
msgstr "%1: %2"

#: package/contents/ui/PowerProfileItem.qml:261
#, kde-kuit-format
msgid ""
"Power profiles may be supported on your device.<nl/>Try installing the "
"<command>power-profiles-daemon</command> package using your distribution's "
"package manager and restarting the system."
msgstr ""
"Energiprofiloj povas esti subtenataj en via aparato.<nl/>Provu instali la "
"pakon <command>power-profiles-daemon</command> uzante la pakadministrilon de "
"via distribuado kaj rekomencante la sistemon."
