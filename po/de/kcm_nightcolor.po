# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Burkhard Lück <lueck@hube-lueck.de>, 2018, 2019, 2020, 2021.
# Frederik Schwarzer <schwarzer@kde.org>, 2020, 2021.
# Frank Steinmetzger <Warp_7@gmx.de>, 2020.
# Alois Spitzbart <spitz234@hotmail.com>, 2022.
# SPDX-FileCopyrightText: 2023 Johannes Obermayr <johannesobermayr@gmx.de>
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-07 00:38+0000\n"
"PO-Revision-Date: 2023-12-18 16:11+0100\n"
"Last-Translator: Johannes Obermayr <johannesobermayr@gmx.de>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.08.4\n"

#: ui/DayNightView.qml:110
#, kde-format
msgid ""
"Color temperature begins changing to night time at %1 and is fully changed "
"by %2"
msgstr ""
"Farbtemperatur wird ab %1 auf die Nachtzeit wechseln und ist bis %2 "
"vollständig umgestellt."

#: ui/DayNightView.qml:113
#, kde-format
msgid ""
"Color temperature begins changing to day time at %1 and is fully changed by "
"%2"
msgstr ""
"Farbtemperatur wird ab %1 auf die Tageszeit wechseln und ist bis %2 "
"vollständig umgestellt."

#: ui/LocationsFixedView.qml:39
#, kde-format
msgctxt ""
"@label:chooser Tap should be translated to mean touching using a touchscreen"
msgid "Tap to choose your location on the map."
msgstr "Tippen, um Ihren Standort auf der Karte auszuwählen."

#: ui/LocationsFixedView.qml:40
#, kde-format
msgctxt ""
"@label:chooser Click should be translated to mean clicking using a mouse"
msgid "Click to choose your location on the map."
msgstr "Klicken, um Ihren Standort auf der Karte auszuwählen."

#: ui/LocationsFixedView.qml:79 ui/LocationsFixedView.qml:104
#, kde-format
msgid "Zoom in"
msgstr "Vergrößern"

#: ui/LocationsFixedView.qml:210
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Modified from <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>World location map</link> by "
"TUBS / Wikimedia Commons / <link url='https://creativecommons.org/licenses/"
"by-sa/3.0'>CC BY-SA 3.0</link>"
msgstr ""
"Von <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>Positionskarte der Welt</link> "
"geändert durch TUBS / Wikimedia Commons / <link url='https://creativecommons."
"org/licenses/by-sa/3.0'>CC BY-SA 3.0</link>"

#: ui/LocationsFixedView.qml:223
#, kde-format
msgctxt "@label: textbox"
msgid "Latitude:"
msgstr "Breitengrad:"

#: ui/LocationsFixedView.qml:250
#, kde-format
msgctxt "@label: textbox"
msgid "Longitude:"
msgstr "Längengrad:"

#: ui/main.qml:98
#, kde-format
msgid "The blue light filter makes the colors on the screen warmer."
msgstr "Durch Blaulichtfilter werden die Bildschirmfarben wärmer dargestellt."

#: ui/main.qml:154
#, kde-format
msgid "Switching times:"
msgstr "Umschaltzeiten:"

#: ui/main.qml:157
#, kde-format
msgid "Always off"
msgstr "Immer aus"

#: ui/main.qml:158
#, kde-format
msgid "Sunset and sunrise at current location"
msgstr "Sonnenunter- und -aufgang am aktuellen Standort"

#: ui/main.qml:159
#, kde-format
msgid "Sunset and sunrise at manual location"
msgstr "Sonnenunter- und -aufgang am benutzerdefinierten Standort"

#: ui/main.qml:160
#, kde-format
msgid "Custom times"
msgstr "Benutzerdefinierte Uhrzeiten"

#: ui/main.qml:161
#, kde-format
msgid "Always on night light"
msgstr "Nachtlicht immer an"

#: ui/main.qml:184
#, kde-format
msgid "Day light temperature:"
msgstr "Tageslichttemperatur:"

#: ui/main.qml:227 ui/main.qml:289
#, kde-format
msgctxt "Color temperature in Kelvin"
msgid "%1K"
msgstr "%1 K"

#: ui/main.qml:232 ui/main.qml:294
#, kde-format
msgctxt "Night colour blue-ish; no blue light filter activated"
msgid "Cool (no filter)"
msgstr "Kühl (Kein Filter)"

#: ui/main.qml:239 ui/main.qml:301
#, kde-format
msgctxt "Night colour red-ish"
msgid "Warm"
msgstr "Warm"

#: ui/main.qml:246
#, kde-format
msgid "Night light temperature:"
msgstr "Nachtlichttemperatur:"

#: ui/main.qml:311
#, kde-format
msgctxt "@label The coordinates for the current location"
msgid "Current location:"
msgstr "Aktueller Ort:"

#: ui/main.qml:317
#, kde-format
msgid "Latitude: %1°   Longitude: %2°"
msgstr "Breitengrad: %1 °   Längengrad: %2 °"

#: ui/main.qml:339
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The device's location will be periodically updated using GPS (if available), "
"or by sending network information to <link url='https://location.services."
"mozilla.com'>Mozilla Location Service</link>."
msgstr ""
"Der Standort des Geräts wird regelmäßig über GPS (falls verfügbar) oder "
"durch Senden von Netzwerkinformationen an den <link url='https://location."
"services.mozilla.com'>Mozilla Location Service</link> aktualisiert."

#: ui/main.qml:357
#, kde-format
msgid "Begin night light at:"
msgstr "Nachtlicht beginnen um:"

#: ui/main.qml:370 ui/main.qml:393
#, kde-format
msgid "Input format: HH:MM"
msgstr "Eingabeformat: HH:MM"

#: ui/main.qml:380
#, kde-format
msgid "Begin day light at:"
msgstr "Tageslicht beginnen um:"

#: ui/main.qml:402
#, kde-format
msgid "Transition duration:"
msgstr "Durchgangsdauer:"

#: ui/main.qml:411
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 Minute"
msgstr[1] "%1 Minuten"

#: ui/main.qml:424
#, kde-format
msgid "Input minutes - min. 1, max. 600"
msgstr "Eingabe in Minuten – min. 1, max. 600"

# Übergangszeiten?
#: ui/main.qml:442
#, kde-format
msgid "Error: Transition time overlaps."
msgstr "Fehler: Übergangszeiten überschneiden sich."

#: ui/main.qml:465
#, kde-format
msgctxt "@info:placeholder"
msgid "Locating…"
msgstr "Lokalisieren ..."

#~ msgctxt "Night colour blue-ish"
#~ msgid "Cool"
#~ msgstr "Kalt"

#, fuzzy
#~| msgid "Activate Night Color"
#~ msgid "Activate blue light filter"
#~ msgstr "Nachtfarben aktivieren"

#~ msgid "Turn on at:"
#~ msgstr "Einschalten um:"

#~ msgid "Turn off at:"
#~ msgstr "Ausschalten um:"

#, fuzzy
#~| msgid "Night Color begins changing back at %1"
#~ msgid "Night Color begins changing back at %1 and ends at %2"
#~ msgstr "Nachtfarben werden ab %1 zurückgesetzt"

#~ msgid "Error: Morning is before evening."
#~ msgstr "Fehler: Der Morgen liegt vor dem Abend."

#~ msgid "Night Color begins at %1"
#~ msgstr "Nachtfarben beginnen um %1"

#~ msgid "Color fully changed at %1"
#~ msgstr "Farbe vollständig umgestellt um %1"

#~ msgid "Normal coloration restored by %1"
#~ msgstr "Normale Farben sind um %1 wiederhergestellt"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Burkhard Lück, Frank Steinmetzger"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "lueck@hube-lueck.de, Warp_7@gmx.de"

#~ msgid "Night Color"
#~ msgstr "Nachtfarben"

#~ msgid "Roman Gilg"
#~ msgstr "Roman Gilg"

#~ msgid " K"
#~ msgstr " K"

#~ msgid "Operation mode:"
#~ msgstr "Operationsmodus"

#~ msgid "Automatic"
#~ msgstr "Automatisch"

#~ msgid "Times"
#~ msgstr "Zeiten"

#~ msgid "Constant"
#~ msgstr "Konstant"

#~ msgid "Sunrise begins:"
#~ msgstr "Sonnenaufgang um:"

#~ msgid "(Input format: HH:MM)"
#~ msgstr "(Eingabeformat: HH:MM)"

#~ msgid "Sunset begins:"
#~ msgstr "Sonnenuntergang um:"

#~ msgid "...and ends:"
#~ msgstr ".. und endet:"

#~ msgid "Manual"
#~ msgstr "Manuell"

#~ msgid "(HH:MM)"
#~ msgstr "(HH:MM)"
