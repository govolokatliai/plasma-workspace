# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Sveinn í Felli <sv1@fellsnet.is>, 2022.
# Guðmundur Erlingsson <gudmundure@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2023-09-09 10:01+0000\n"
"Last-Translator: Guðmundur Erlingsson <gudmundure@gmail.com>\n"
"Language-Team: Icelandic <kde-i18n-doc@kde.org>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.3\n"

#: contents/ui/BarcodePage.qml:23
#, kde-format
msgid "QR Code"
msgstr "QR-kóði"

#: contents/ui/BarcodePage.qml:24
#, kde-format
msgid "Data Matrix"
msgstr "Gagnafylki"

#: contents/ui/BarcodePage.qml:25
#, kde-format
msgctxt "Aztec barcode"
msgid "Aztec"
msgstr "Aztec"

#: contents/ui/BarcodePage.qml:26
#, kde-format
msgid "Code 39"
msgstr "Kóði 39"

#: contents/ui/BarcodePage.qml:27
#, kde-format
msgid "Code 93"
msgstr "Kóði 93"

#: contents/ui/BarcodePage.qml:28
#, kde-format
msgid "Code 128"
msgstr "Kóði 128"

#: contents/ui/BarcodePage.qml:44
#, kde-format
msgid "Return to Clipboard"
msgstr "Til baka á klippispjaldið"

#: contents/ui/BarcodePage.qml:79
#, kde-format
msgid "Change the QR code type"
msgstr "Breyta gerð QR-kóða"

#: contents/ui/BarcodePage.qml:136
#, kde-format
msgid "Creating QR code failed"
msgstr "Mistókst að búa til QR-kóða"

#: contents/ui/BarcodePage.qml:148
#, kde-format
msgid ""
"There is not enough space to display the QR code. Try resizing this applet."
msgstr ""
"Ekki er nóg pláss til að birta QR-kóðann. Prófaðu að breyta stærð "
"smáforritsins."

#: contents/ui/DelegateToolButtons.qml:26
#, kde-format
msgid "Invoke action"
msgstr "Framkvæma aðgerð"

#: contents/ui/DelegateToolButtons.qml:41
#, kde-format
msgid "Show QR code"
msgstr "Birta QR-kóða"

#: contents/ui/DelegateToolButtons.qml:57
#, kde-format
msgid "Edit contents"
msgstr "Breyta innihaldi"

#: contents/ui/DelegateToolButtons.qml:71
#, kde-format
msgid "Remove from history"
msgstr "Fjarlægja úr vinnsluferli"

#: contents/ui/EditPage.qml:82
#, kde-format
msgctxt "@action:button"
msgid "Save"
msgstr "Vista"

#: contents/ui/EditPage.qml:90
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Hætta við"

#: contents/ui/main.qml:30
#, kde-format
msgid "Clipboard Contents"
msgstr "Innihald klippispjalds"

#: contents/ui/main.qml:31 contents/ui/Menu.qml:134
#, kde-format
msgid "Clipboard is empty"
msgstr "Klippispjald er tómt"

#: contents/ui/main.qml:59
#, kde-format
msgid "Clear History"
msgstr "Hreinsa feril"

#: contents/ui/main.qml:71
#, kde-format
msgid "Configure Clipboard…"
msgstr "Stilla klippispjald…"

#: contents/ui/Menu.qml:134
#, kde-format
msgid "No matches"
msgstr "Engar samsvaranir"

#: contents/ui/UrlItemDelegate.qml:107
#, kde-format
msgctxt ""
"Indicator that there are more urls in the clipboard than previews shown"
msgid "+%1"
msgstr "+%1"

#~ msgid "The QR code is too large to be displayed"
#~ msgstr "QR-kóðinn er of langur til að hægt sé að birta hann"
